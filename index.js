/*
	Client-Server Architecture

	- is a computing model wherein a server hosts, delivers, and manages resources that a client consumes.

	What is a client?

	A client is an application which creates requests for resources from a server. A client will trigger an action, in a web development context, through a URL and wai for the response of the server.

	What is a server?

	A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients.

	What is Node.js

	Node.js is an environment to be able to develop applications with Javascript. With this, we can run JS even without the use of an HTML.

	Javascript was originally conceptualized to be used for Front-End applications. This is the reason why out vanilla Javascript had to be linked and connected to an HTML file before you can run the JS.

	With the advent of Node.js, JS can now be used to create Back-End applications.

		Note: 
			Front-end is usually the page that we're seeing. It is usually a client application that requests resources from a server or a back-end.

			Back-end applications are usually server-related application which handles requests from a front-end or a client.

	Why is Node.js so popular?

		Performance - Nodejs is one of the most performing environment for creating backend application for JS.

		Familiarity - It uses JS as its language and therefore very familiar for most developers.

		NPM - Node Package Manager - Is the largest registry for node packages. These packages are small bits of programs, methods, functions and codes that greatly help in the development of an application.
*/

//console.log("Hello, World!");

/*
	We are now able to run a simple nodejs server. Wherein when we added our URL in the browser, the browser as a client actually requested to our server and our server was able to respond with a text.

	We use require() method to load node.js modules.

		A module is a software component or part of a program which contains one or more routines.

		The http module is a default module from node.js
		
		The http module let node.js transfer data or let our client and server exchange data via Hypertext Transfer Protocol

		protocol => http://localhost:4000 <= server

		With this, the client, which is our browser, automatically created a request and our server was able to respond with a message.

		This is how clients and servers communicate with each other. A client triggers an action from a server via URL and the server responds with the data.

			Note: 

			Messages from a client which triggers an action from a server is called a request.

			Messages from a server to respond to a client's request is called a response.

	http module contains methods and other codes which allowed us to create a server and let our client and server communicate through HTTP.

	modules when imported/required are objects.
*/


let http = require("http");

//console.log(http);

/*
	http.createServer() method allowed us to create a server and handle the requests of a client.

	.createServer() method has an anonymous function that handles our clients request and our server response. The anonymous function in the createServer method is able to receive two objects. First, req or request, this is the request from the client. Second, is res, this is our server's response.
	Each request and response parameters are objects which contains the details of a request or response as well as methods to handle them.

	res.writeHead() is a method of the response object. This will allow us to add headers, which are additional information about our server's response. 'Content-Type' is one of the more recognizable headers, it is pertaining to the data type of the content we are responding with. The first argument in writeHead is an HTTP status which is used to tell the client about the status of their request. 200 meaning OK. HTTP 404 - means the resource you're trying to access cannot be found. 403 - means the resource you're trying to access is forbidden or requires the proper authentication.

	res.end() is a method of the response objects which ends the server's response and send a message/data as a string.

	.listen() allows us to assign a port to a server. There are several tasks and processes in our computers which are also designated into their specific ports.

	port is a virtual point where connections start and end.
	http://localhost:4000 - localhost is your current machine and 4000 is the port number assigned to where the process/server is listening/running from. This is the port where our server is ran. port 4000 - popularly used for backend applications. port 8000, 5000 are available.
*/

http.createServer(function(req,res){

	/*
		We can actually respond differently to different requests.

		When our browser requests through server the complete URL is like this:

		http://localhost:4000/ - is called an endpoint.

		We can respond differently to different request URL endpoints.
	*/
	//.url is a property of the request object pertaining to the endpoint of the URL.
	// / - default endpoint
	//console.log(req.url);
	//How we can respond to different requests is by handling client request by endpoints. This is called routing.

	if(req.url === "/"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('Hi! My Name is Adrian');
	} else if (req.url === "/login"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('Welcome to the Login Page');
	} else {
		//How can we respond to requests with URL endpoints that we don't have a route for?
			//This will be our response if an endpoint passed in the client's request URL is not recognized or there is no designated route for that endpoint.
		res.writeHead(404,{'Content-Type': 'text/plain'});
		res.end('Resource cannot be found.');
	}
	

}).listen(4000);

console.log("Server is running on localHost:4000");